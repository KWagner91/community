package de.awacademy.com.Backend.entry;

import de.awacademy.com.Backend.comment.CommentRepository;
import de.awacademy.com.Backend.userAccount.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EntryService {

    @Autowired
    EntryRepository entryRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    CommentRepository commentRepository;

    public void saveEntry(EntryDTO entryDTO) {
            entryRepository.save(new Entry(entryDTO.getTitle(),entryDTO.getText(),entryDTO.getType(),
                    userRepository.findByUsername(entryDTO.getUserName()).get()));
    }

    public List<EntryDTO> getEntries() {
        List<EntryDTO> entries = new ArrayList<>();
        for (Entry entry : entryRepository.findAll()) {
            entries.add(new EntryDTO(entry.getId(), entry.getTitle(), entry.getText(), entry.getType(),
                    entry.getUser().getUsername()));
        }
        return entries;
    }

    public EntryDTO getEntry(Long id) {
        Optional<Entry> optionalEntry = entryRepository.findById(id);
        return new EntryDTO( optionalEntry.get().getId(), optionalEntry.get().getTitle(), optionalEntry.get().getText(), optionalEntry.get().getType(),
                optionalEntry.get().getUser().getUsername());
    }

    public void deleteEntry(long id) {
        commentRepository.deleteAllByEntry_Id(id);
        entryRepository.deleteById(id);
    }

    public List<EntryDTO> getUserEntries(String userName) {
        Long userId = userRepository.findByUsername(userName).get().getId();
        List<EntryDTO> userEntries = new ArrayList<>();

        for (Entry entry : entryRepository.findAllByUser_Id(userId)) {
            userEntries.add(new EntryDTO(entry.getId(), entry.getTitle(),entry.getText(), entry.getType(), entry.getUser().getUsername()));
        }
        return userEntries;
    }

    public void editEntry(EntryDTO entryDTO) {
        // findById in Kombination mit .get() ist eine Standard-CRUD-Methode, die hier den bestehenden Eitnrag aus der Datenbank holt
        Entry editedEntry = entryRepository.findById(entryDTO.getId()).get();
        editedEntry.setText(entryDTO.getText());
        editedEntry.setTitle(entryDTO.getTitle());
        editedEntry.setType(entryDTO.getType());
        entryRepository.save(editedEntry);
    }
}
