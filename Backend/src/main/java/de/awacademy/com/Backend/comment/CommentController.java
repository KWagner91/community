package de.awacademy.com.Backend.comment;

import de.awacademy.com.Backend.entry.EntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CommentController {

    @Autowired
    CommentService commentService;

    @Autowired
    EntryService entryService;

    @GetMapping("/api/comments")
    public List<CommentDTO> getComments(@RequestParam Long id) {
        return commentService.getComments(id);
    }

    @GetMapping("/api/deleteComment")
    public void deleteComment(@RequestParam Long id) {
        commentService.deleteComment(id);
    }

    @PostMapping("/api/comment")
    public void saveComment(@RequestBody CommentDTO commentDTO) {
       commentService.saveComment(commentDTO);
    }

}
