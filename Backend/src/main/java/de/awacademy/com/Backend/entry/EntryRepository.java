package de.awacademy.com.Backend.entry;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EntryRepository extends CrudRepository<Entry, Long> {
    // Für die Anzahl der Gesuche und Angebote bei Google-Map-Window
    List<Entry> findEntriesByUser_IdAndType(long id, String type);
    List<Entry> findAllByUser_Id(Long userId);
}
