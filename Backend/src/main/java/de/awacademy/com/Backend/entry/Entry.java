package de.awacademy.com.Backend.entry;

import de.awacademy.com.Backend.comment.Comment;
import de.awacademy.com.Backend.userAccount.UserAccount;

import javax.persistence.*;
import java.util.List;

@Entity
public class Entry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    @Column(columnDefinition = "TEXT")
    private String text;
    private String type;

    @ManyToOne
    private UserAccount user;

    @OneToMany(mappedBy = "entry")
    List<Comment> comments;

    public Entry() {}

    public Entry(String title, String text, String type, UserAccount user) {
        this.title = title;
        this.text = text;
        this.type = type;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public UserAccount getUser() {
        return user;
    }

    public void setUser(UserAccount user) {
        this.user = user;
    }

    public List<Comment> getComments() {
        return comments;
    }
}
