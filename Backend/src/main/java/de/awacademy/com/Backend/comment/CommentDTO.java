package de.awacademy.com.Backend.comment;

public class CommentDTO {

    private Long id;
    private Long entryId;
    private String text;
    private String userName;

    public CommentDTO() {
    }

    public CommentDTO(Long id, String text, String userName, Long entryId) {
        this.id = id;
        this.text = text;
        this.userName = userName;
        this.entryId = entryId;
    }

    public Long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String getUserName() {
        return userName;
    }

    public Long getEntryId() {
        return entryId;
    }
}
