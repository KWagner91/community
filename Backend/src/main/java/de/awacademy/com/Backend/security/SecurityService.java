package de.awacademy.com.Backend.security;

import de.awacademy.com.Backend.userAccount.UserAccount;
import de.awacademy.com.Backend.userAccount.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SecurityService implements UserDetailsService {

    private UserRepository userRepo;

    @Autowired
    public SecurityService(UserRepository userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserAccount optionalUser = userRepo.findByUsername(username).get();

        if(username.equalsIgnoreCase(optionalUser.getUsername())) {
            return new User(optionalUser.getUsername(), optionalUser.getPassword(), List.of());
        }

        throw new UsernameNotFoundException("Username nicht vorhanden");
    }





}
