package de.awacademy.com.Backend.userAccount;

public class UserMarkerDTO {

    private String username;
    private double longitude;
    private double latitude;
    private int request;
    private int offer;

    public UserMarkerDTO(String username, double longitude, double latitude, int request, int offer) {
        this.username = username;
        this.longitude = longitude;
        this.latitude = latitude;
        this.request = request;
        this.offer = offer;
    }

    public String getUsername() {
        return username;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public int getRequest() {
        return request;
    }

    public int getOffer() {
        return offer;
    }
}
