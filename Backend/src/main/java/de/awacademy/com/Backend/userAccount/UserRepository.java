package de.awacademy.com.Backend.userAccount;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<UserAccount, Long> {
//    List<UserAccount> findAll();
//    Optional<UserAccount> findById(long id);
    Optional<UserAccount> findByUsername(String username);
}
