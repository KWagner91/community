package de.awacademy.com.Backend.entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class EntryController {

    @Autowired
    EntryService entryService;

    @GetMapping("/api/allEntries")
    public List<EntryDTO> getEntries() {
        return entryService.getEntries();
    }

    // RequestParam = Info aus dem Header ; RequestBody = Info aus dem Body;
    @GetMapping("/api/userEntries")
    public List<EntryDTO> getUserEntries(@RequestParam String userName) {
        return entryService.getUserEntries(userName);
    }

    @GetMapping("/api/entry")
    public EntryDTO getEntry(@RequestParam Long id) {
        return entryService.getEntry(id);
    }

    @PostMapping("/api/entry")
    public void saveEntry(@RequestBody EntryDTO entryDTO) {
        entryService.saveEntry(entryDTO);
    }

    @PostMapping("/api/deleteEntry")
    public void deleteEntry(@RequestBody long id) {
        entryService.deleteEntry(id);
    }

    @PostMapping("/api/editEntry")
    public void editEntry(@RequestBody EntryDTO entryDTO){
        entryService.editEntry(entryDTO);
    }
}
