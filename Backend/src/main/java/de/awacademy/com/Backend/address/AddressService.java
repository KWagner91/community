package de.awacademy.com.Backend.address;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AddressService {

    @Autowired
    AddressRepository addressRepository;

    public Optional<Address> findAddressForRegistration(String street, String streetNumber, String postalcode) {
        return addressRepository.findByStreetAndStreetNumberAndAndPostalcode(street, streetNumber, postalcode);
    }

    public void save(Address address) {
        addressRepository.save(address);
    }

    public Optional<Address> findById(int id) {
        return addressRepository.findById(id);
    }
}
