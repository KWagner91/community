package de.awacademy.com.Backend.userAccount;

public class UserProfileDTO {
    private Long id;
    private String userName;
    private String street;
    private String streetNumber;
    private String postalcode;
    private String city;
    private int offers;
    private int requests;

    public UserProfileDTO() {
    }

    public UserProfileDTO(Long id, String userName, String street, String streetNumber, String postalcode, String city, int offers, int requests) {
        this.id = id;
        this.userName = userName;
        this.street = street;
        this.streetNumber = streetNumber;
        this.postalcode = postalcode;
        this.city = city;
        this.offers = offers;
        this.requests = requests;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getOffers() {
        return offers;
    }

    public void setOffers(int offers) {
        this.offers = offers;
    }

    public int getRequests() {
        return requests;
    }

    public void setRequests(int requests) {
        this.requests = requests;
    }
}
