package de.awacademy.com.Backend.userAccount;

import de.awacademy.com.Backend.address.Address;
import de.awacademy.com.Backend.address.AddressService;
import de.awacademy.com.Backend.entry.EntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private PasswordEncoder passwordEncoder;
    private UserRepository userRepository;
    private EntryRepository entryRepository;
    private AddressService addressService;

    @Autowired
    public UserService(PasswordEncoder passwordEncoder, UserRepository userRepository, EntryRepository entryRepository, AddressService addressService) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.entryRepository = entryRepository;
        this.addressService = addressService;
    }


    public void registerUser(RegisterDTO registerDTO) {
        String encodedpassword = passwordEncoder.encode(registerDTO.getPassword());
        // if-Abfrage: falls die Adresse bereits in Datenbank vorhanden ist, bekommt der neue User das bestehende Adressobjekt zugewiesen
        if (addressService.findAddressForRegistration(registerDTO.getStreet(), registerDTO.getStreetNumber(),
                registerDTO.getPostalcode()).isPresent()) {
            userRepository.save(new UserAccount(registerDTO.getUsername(), encodedpassword,
                    addressService.findAddressForRegistration(registerDTO.getStreet(), registerDTO.getStreetNumber(),
                            registerDTO.getPostalcode()).get(), registerDTO.getLatitude(), registerDTO.getLongitude()));
        } else {
            addressService.save(new Address(registerDTO.getStreet(), registerDTO.getStreetNumber(),
                    registerDTO.getPostalcode(), registerDTO.getCity()));
            userRepository.save(new UserAccount(registerDTO.getUsername(), encodedpassword,
                    addressService.findAddressForRegistration(registerDTO.getStreet(),
                            registerDTO.getStreetNumber(), registerDTO.getPostalcode()).get(),registerDTO.getLatitude(), registerDTO.getLongitude()));
        }
    }

    public List<UserMarkerDTO> getUserMarkerList() {
        List<UserMarkerDTO> userMarkers = new LinkedList<>();
        for (UserAccount user : userRepository.findAll()) {
            int entryRequest = entryRepository.findEntriesByUser_IdAndType(user.getId(), "request").size();
            int entryOffer = entryRepository.findEntriesByUser_IdAndType(user.getId(), "offer").size();
            userMarkers.add(new UserMarkerDTO(user.getUsername(), user.getLongitude(), user.getLatitude(), entryRequest, entryOffer));
        }
        return userMarkers;
        }

        // Validation für Username
    public boolean checkIfUserNamePresent (String username) {
        return userRepository.findByUsername(username).isPresent();
    }

    public UserProfileDTO getUserProfileByName(String userName) {
        Optional<UserAccount> optionalUser = userRepository.findByUsername(userName);
        Optional<Address> optionalAddress = addressService.findById(optionalUser.get().getAddress().getId());
        return new UserProfileDTO(optionalUser.get().getId(),optionalUser.get().getUsername(),optionalAddress.get().getStreet(),
                optionalAddress.get().getStreetNumber(),optionalAddress.get().getPostalcode(),optionalAddress.get().getCity(),
                entryRepository.findEntriesByUser_IdAndType(optionalUser.get().getId(), "offer").size(),
                entryRepository.findEntriesByUser_IdAndType(optionalUser.get().getId(), "request").size());
    }
}

