package de.awacademy.com.Backend.comment;

import de.awacademy.com.Backend.comment.Comment;
import de.awacademy.com.Backend.comment.CommentDTO;
import de.awacademy.com.Backend.comment.CommentRepository;
import de.awacademy.com.Backend.entry.Entry;
import de.awacademy.com.Backend.entry.EntryRepository;
import de.awacademy.com.Backend.userAccount.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommentService {

    @Autowired
    CommentRepository commentRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    EntryRepository entryRepository;


    public void saveComment(CommentDTO commentDTO) {
        commentRepository.save(new Comment(commentDTO.getText(), commentDTO.getUserName(),
              entryRepository.findById(commentDTO.getEntryId()).get()));   // .get() holt eigentliches commentDTO-Objekt aus Optional
    }

    public List<CommentDTO> getComments(Long id) {
        List<CommentDTO> comments = new ArrayList<>();
        for (Comment comment : commentRepository.findAllByEntry_Id(id)) {
            comments.add(new CommentDTO(comment.getId(), comment.getText(), comment.getUser(), comment.getEntry().getId()));
        }
        return comments;
    }

    public void deleteComment(Long id) {
        commentRepository.deleteById(id);
    }
}
