package de.awacademy.com.Backend.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;


@RestController
public class SecurityController {

    @GetMapping("/api/sessionUser")
    public UserDTO sessionUser(@AuthenticationPrincipal UserDetails userDetails) {
        if (userDetails == null){
            return null;
        }
        return new UserDTO(userDetails.getUsername());
    }

    @PostMapping("/api/logout")
    public void logoutUser(HttpSession session ) {
            session.invalidate();
    }
}
