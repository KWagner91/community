package de.awacademy.com.Backend.address;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AddressRepository extends CrudRepository<Address, Integer> {
    Optional<Address> findByStreetAndStreetNumberAndAndPostalcode(String street, String streetNumber, String postalcode);
}
