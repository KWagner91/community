package de.awacademy.com.Backend.userAccount;

//import de.awacademy.com.Backend.comment.Comment;
import de.awacademy.com.Backend.address.Address;
import de.awacademy.com.Backend.entry.Entry;

import javax.persistence.*;
import java.util.List;

@Entity
public class UserAccount {
 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;
    private String password;
    private double longitude;
    private double latitude;

    @OneToMany(mappedBy = "user")
    private List<Entry> entries;

    @ManyToOne
    private Address address;


    public UserAccount() {}

    public UserAccount(String username, String password, Address address, double latitude, double longitude) {
        this.username = username;
        this.password = password;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public List<Entry> getEntries() {
        return entries;
    }

    public Address getAddress() {
        return address;
    }
}
