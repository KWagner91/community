package de.awacademy.com.Backend.userAccount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.naming.AuthenticationException;
import java.util.List;

@RestController
public class UserController {

    UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/api/registration")
    public void register(@RequestBody RegisterDTO registerDTO) throws AuthenticationException {
        if (userService.checkIfUserNamePresent(registerDTO.getUsername())) {
            throw new AuthenticationException("Diesen Usernamen gibt es schon!");
        }
        else {
            userService.registerUser(registerDTO);
        }
    }

    @GetMapping("/api/userlist")
    public List<UserMarkerDTO> getUserList() {
        return userService.getUserMarkerList();
    }

    @GetMapping("/api/user")
    // Methode für die Profilseite eines Users
    public UserProfileDTO getUser(@RequestParam String userName) {
        return userService.getUserProfileByName(userName);
    }
}
