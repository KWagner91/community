package de.awacademy.com.Backend.comment;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CommentRepository extends CrudRepository<Comment, Long> {
    void deleteAllByEntry_Id(Long id);
    // Gibt die Kommentare zu einem Eintrag zurück
    List<Comment> findAllByEntry_Id(Long id);
}
