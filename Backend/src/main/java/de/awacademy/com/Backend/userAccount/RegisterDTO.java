package de.awacademy.com.Backend.userAccount;

public class RegisterDTO {
    private String username;
    private String password;
    private String street;
    private String streetNumber;
    private String postalcode;
    private String city;
    private double latitude;
    private double longitude;

    public RegisterDTO(String username, String password, String street, String streetNumber, String postalcode, String city, double latitude, double longitude) {
        this.username = username;
        this.password = password;
        this.street = street;
        this.streetNumber = streetNumber;
        this.postalcode = postalcode;
        this.city = city;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getStreet() {
        return street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public String getCity() {
        return city;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
