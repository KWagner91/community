package de.awacademy.com.Backend.address;

import de.awacademy.com.Backend.userAccount.UserAccount;

import javax.persistence.*;
import java.util.List;

@Entity
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String street;
    private String streetNumber;
    private String postalcode;
    private String city;

    @OneToMany(mappedBy = "address")
    private List<UserAccount> userAccounts;

    public Address() {
    }

    public Address(String street, String streetNumber, String postalcode, String city) {
        this.street = street;
        this.streetNumber = streetNumber;
        this.postalcode = postalcode;
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public String getStreet() {
        return street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public String getCity() {
        return city;
    }

    public List<UserAccount> getUserAccounts() {
        return userAccounts;
    }
}
