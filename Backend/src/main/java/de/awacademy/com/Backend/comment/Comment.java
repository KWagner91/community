package de.awacademy.com.Backend.comment;

import de.awacademy.com.Backend.entry.Entry;
import de.awacademy.com.Backend.userAccount.UserAccount;

import javax.persistence.*;

@Entity
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition = "TEXT")
    private String text;
    private String user;

    @ManyToOne
    private Entry entry;

    public Comment() {
    }

    public Comment(String text, String user, Entry entry) {
        this.text = text;
        this.user = user;
        this.entry = entry;
    }

    public Long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String getUser() {
        return user;
    }

    public Entry getEntry() {
        return entry;
    }

}
