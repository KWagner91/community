package de.awacademy.com.Backend.entry;

public class EntryDTO {

    private long id;
    private String title;
    private String text;
    private String type;
    private String userName;

    public EntryDTO() {}

    public EntryDTO(long id, String title, String text, String type, String userName) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.type = type;
        this.userName = userName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public long getId() {
        return id;
    }
}
