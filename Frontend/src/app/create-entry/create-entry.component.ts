import { Component, OnInit } from '@angular/core';
import {SecurityService} from '../security.service';
import {HttpClient} from '@angular/common/http';
import {User} from '../user';
import {Router} from '@angular/router';
import {EntryService} from '../entry.service';
import {EntryDTO} from '../entryDTO';

@Component({
  selector: 'app-create-entry',
  templateUrl: './create-entry.component.html',
  styleUrls: ['./create-entry.component.css']
})
export class CreateEntryComponent implements OnInit {

  sessionUser: User|null;

  entryDTO = {
    id: 0,
    title: '',
    text: '',
    type: '',
    userName: ''
  };

  constructor(private securityService: SecurityService,
              private httpClient: HttpClient, private router: Router, private entryService: EntryService) { }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
  }

  saveEntry() {
    this.entryDTO.userName = this.securityService.getUserName();
    this.entryService.addEntry(this.entryDTO);
    this.entryDTO = {id: 0, title: '', text: '', type: '', userName: ''};
  }

}



