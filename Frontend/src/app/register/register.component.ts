import {Component, OnInit} from '@angular/core';
import {User} from '../user';
import {SecurityService} from '../security.service';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {RegisterDTO} from '../registerDTO';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  sessionUser: User;

  registerDTO: RegisterDTO = {
    username: '',
    password: '',
    street: '',
    streetNumber: '',
    postalcode: '',
    city: '',
    latitude: 0,
    longitude: 0
  };

  constructor(private securityService: SecurityService, private httpClient: HttpClient, private router: Router) {
  }

  ngOnInit() {
  }

  getCoordinates() {
    let geocodes = null;
    this.httpClient.get<JSON>(this.getGoogleURL()).subscribe(r => {
      geocodes = r;
      console.log(geocodes);
      this.registerDTO.latitude = geocodes.results[0].geometry.location.lat;
      this.registerDTO.longitude = geocodes.results[0].geometry.location.lng;
    });
  }

  register() {
    this.getCoordinates();

    setTimeout( () => {
      console.log(this.registerDTO);
      this.securityService.register(this.registerDTO);
      this.registerDTO = {
        username: '',
        password: '',
        street: '',
        streetNumber: '',
        postalcode: '',
        city: '',
        latitude: 0,
        longitude: 0
      };
    }, 1000);

    this.router.navigate(['/login']);
  }

  getGoogleURL() {
    // tslint:disable-next-line:max-line-length
    return 'https://maps.googleapis.com/maps/api/geocode/json?address=' + this.registerDTO.street + '+' + this.registerDTO.streetNumber + ',+' + this.registerDTO.city + ',+Deutschland&key=AIzaSyCtYSODQt7swOobRBnXKEXA90ke2SLFHE4';
  }
}
