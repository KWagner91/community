export interface CommentDTO {
  entryId: number;
  text: string;
  userName: string;
}

