export interface RegisterDTO {
  username: string;
  password: string;
  street: string;
  streetNumber: string;
  postalcode: string;
  city: string;
  latitude: number;
  longitude: number;
}
