export interface EntryDTO {
  id: number;
  title: string;
  text: string;
  type: string;
  userName: string;
}
