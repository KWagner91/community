import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {EntryDTO} from './entryDTO';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class EntryService {

  constructor(private httpClient: HttpClient, private router: Router) {
  }

  deleteEntry(id: number) {
    this.httpClient.post('/api/deleteEntry', id).subscribe();
    document.location.reload();
  }

  editEntry(entryDTO: EntryDTO) {
    this.httpClient.post('/api/editEntry', entryDTO).subscribe();
    this.router.navigate(['/mainPage']);
  }

  addEntry(entryDTO: EntryDTO) {
    this.httpClient.post('/api/entry', entryDTO).subscribe();
    this.router.navigate(['/mainPage']);
  }
}
