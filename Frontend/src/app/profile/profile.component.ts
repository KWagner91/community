import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UserProfileDTO} from '../userProfileDTO';
import {UserService} from '../user.service';
import {HttpClient, HttpParams} from '@angular/common/http';
import {User} from '../user';
import {SecurityService} from '../security.service';
import {EntryDTO} from '../entryDTO';
import {EntryService} from '../entry.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  private sessionUser: User | null;
  user: UserProfileDTO = null;
  entries: EntryDTO[] = [];

  output = {
    all: 'all',
    offers: '',
    request: '',
  };


  constructor(private userService: UserService, private route: ActivatedRoute, private httpClient: HttpClient,
              private securityService: SecurityService, private entryService: EntryService) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );

    this.route.params.subscribe(userName => {
      this.httpClient.get<UserProfileDTO>('/api/user', {params: userName})
        .subscribe(u => this.user = u);
      this.httpClient.get<EntryDTO[]>('/api/userEntries', {params: userName}).subscribe(el => this.entries = el);
    });
    console.log(this.entries);
  }

  showContent(status: string) {
    if (status === 'offers') {
      this.output.offers = status;
      this.output.all = null;
      this.output.request = null;
    } else if (status === 'requests') {
      this.output.request = status;
      this.output.all = null;
      this.output.offers = null;
      console.log(this.output);
    } else {
      this.output.all = 'all';
      this.output.offers = null;
      this.output.request = null;
    }
  }

}
