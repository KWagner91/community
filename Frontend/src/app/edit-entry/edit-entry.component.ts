import { Component, OnInit } from '@angular/core';
import {EntryDTO} from '../entryDTO';
import {ActivatedRoute} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {User} from '../user';
import {SecurityService} from '../security.service';
import {EntryService} from '../entry.service';

@Component({
  selector: 'app-edit-entry',
  templateUrl: './edit-entry.component.html',
  styleUrls: ['./edit-entry.component.css']
})
export class EditEntryComponent implements OnInit {

  private sessionUser: User | null;

  private entryDTO: EntryDTO;

  constructor(private route: ActivatedRoute,
              private httpClient: HttpClient, private securityService: SecurityService, private entryService: EntryService) { }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u);

    // erstes Subscribe holt ID aus URL und übergibt es als Parameter dem get-Request
    // zweites Subscribe speichert das Objekt aus dem Backend in unserer Instanzvariable entryDTO im Frontend
    this.route.params.subscribe(idParam => {
      this.httpClient.get<EntryDTO>('/api/entry', {params: idParam}).subscribe(e => this.entryDTO = e);
    });
  }

  saveEntry() {
    this.entryService.editEntry(this.entryDTO);
  }
}
