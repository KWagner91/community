import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RegisterComponent} from './register/register.component';
import {LoginComponent} from './login/login.component';
import {MainPageComponent} from './main-page/main-page.component';
import {EntriesComponent} from './main-page/entries/entries.component';
import {CreateEntryComponent} from './create-entry/create-entry.component';
import {CommentsComponent} from './main-page/entry-page/comments/comments.component';
import {EntryPageComponent} from './main-page/entry-page/entry-page.component';
import {ProfileComponent} from './profile/profile.component';
import {AboutUsComponent} from './about-us/about-us.component';
import {EditEntryComponent} from './edit-entry/edit-entry.component';



const routes: Routes = [
  { path: '', redirectTo: 'mainPage', pathMatch: 'full'},
  { path: 'mainPage', component: MainPageComponent, children: [
      { path: '', redirectTo: 'entries', pathMatch: 'full'},
      { path: 'entries', component: EntriesComponent},
      { path: 'entries/entryPage/:id', component: EntryPageComponent},
    ]},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'request', component: CreateEntryComponent },
  { path: 'comment', component: CommentsComponent },
  { path: 'createEntry', component: CreateEntryComponent },
  { path: 'profile/:userName', component: ProfileComponent },
  { path: 'mainPage/entries/profile/:userName', component: ProfileComponent, pathMatch: 'prefix' },
  { path: 'aboutUs', component: AboutUsComponent },
  { path: 'mainPage/entries/editEntry/:id', component: EditEntryComponent, pathMatch: 'prefix'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
