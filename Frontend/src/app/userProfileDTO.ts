export interface UserProfileDTO {
  id: number;
  userName: string;
  street: string;
  streetNumber: string;
  postalcode: string;
  city: string;
  offers: number;
  requests: number;
}
