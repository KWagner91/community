import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {EntryDTO} from '../../entryDTO';
import {EntryService} from '../../entry.service';
import {HttpClient} from '@angular/common/http';
import {CommentService} from '../entry-page/comments/comment.service';
import {SecurityService} from '../../security.service';
import {User} from '../../user';

@Component({
  selector: 'app-entries',
  templateUrl: './entries.component.html',
  styleUrls: ['./entries.component.css']
})

export class EntriesComponent implements OnInit, OnChanges {

  private sessionUser: User | null;

  output = {
    all: 'all',
    offers: '',
    request: '',
  };

  entries: EntryDTO[] = [];

  constructor(private entryService: EntryService, private httpClient: HttpClient, private commentService: CommentService,
              private securityService: SecurityService) {
  }

  ngOnInit() {
    this.httpClient.get<EntryDTO[]>('/api/allEntries').subscribe(el => this.entries = el);

    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.httpClient.get<EntryDTO[]>('/api/allEntries').subscribe(el => this.entries = el);
  }

  showContent(status: string) {
    if (status === 'offers') {
      this.output.offers = status;
      this.output.all = null;
      this.output.request = null;
    } else if (status === 'requests') {
      this.output.request = status;
      this.output.all = null;
      this.output.offers = null;
      console.log(this.output);
    } else {
      this.output.all = 'all';
      this.output.offers = null;
      this.output.request = null;
    }
  }
}

