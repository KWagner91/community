import { Component, OnInit } from '@angular/core';
import {SecurityService} from '../security.service';
import {HttpClient} from '@angular/common/http';
import {User} from '../user';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  sessionUser: User|null = null;

  entryDTO = {
    title: '',
    text: '',
    type: '',
    userName: ''
  };

  constructor(private securityService: SecurityService, private httpClient: HttpClient) { }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
  }

  saveEntry() {
    this.entryDTO.userName = this.securityService.getUserName();
    console.log(this.entryDTO);
    this.httpClient.post('/api/entry', this.entryDTO).subscribe();
    this.entryDTO = {title: '', text: '', type: '', userName: ''};
    // todo: switch to onChange
    // window.location.reload();
  }
}
