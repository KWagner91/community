export interface UserMarkerDTO {
  username: string;
  longitude: number;
  latitude: number;
  request: number;
  offer: number;
}
