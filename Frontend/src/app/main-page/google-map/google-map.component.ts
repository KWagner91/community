/* https://timdeschryver.dev/posts/google-maps-as-an-angular-component */

import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UserMarkerDTO} from './userMarkerDTO';
import {GoogleMap, MapInfoWindow, MapMarker} from '@angular/google-maps';
import {timeout} from 'rxjs/operators';

@Component({
  selector: 'app-google-map',
  templateUrl: './google-map.component.html',
  styleUrls: ['./google-map.component.css']
})
export class GoogleMapComponent implements OnInit {
  @ViewChild(GoogleMap, { static: false }) map: GoogleMap;
  @ViewChild(MapInfoWindow, { static: false }) info: MapInfoWindow;
  zoom = 11;
  center = {
    lat: 48.1351,
    lng: 11.5820,
  };
  options: google.maps.MapOptions = {
    disableDoubleClickZoom: true,
    minZoom: 2,
  };
  infoContent = '';
  infoRequest = '';
  infoOffer = '';
  userList: UserMarkerDTO[] = [];
  markers = [];

  openInfo(marker: MapMarker, content, request, offer) {
    this.infoContent = content;
    this.infoRequest = request;
    this.infoOffer = offer;
    this.info.open(marker);
  }

  constructor(private httpClient: HttpClient) {}

  ngOnInit() {
   setTimeout(() => this.getUserMarkers(), 500 );
  }

  getUserMarkers() {
    this.httpClient.get<UserMarkerDTO[]>('/api/userlist').subscribe(ul => this.userList = ul);
    for (const user of this.userList) {
      this.markers.push({
          position: {
            lat: user.latitude,
            lng: user.longitude,
          },
          title: user.username,
          offer: user.offer,
          request: user.request,
        }
      );
    }
  }
}
