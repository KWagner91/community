import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {CommentDTO} from '../../../commentDTO';


@Injectable({
  providedIn: 'root'
})

export class CommentService {

  constructor(private httpClient: HttpClient) {
  }

  addComment(commentDTO: CommentDTO) {
    this.httpClient.post('/api/comment', commentDTO).subscribe();
  }

  deleteComment(id: number) {
    this.httpClient.get('/api/deleteComment', {params: new HttpParams().set('id', String(id))}).subscribe();
  }
}
