import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CommentService} from './comment.service';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../../../user';
import {SecurityService} from '../../../security.service';
import {Comment} from './comment';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit, OnChanges {

  private sessionUser: User | null;

  comments: Comment[] = [];

  constructor(private commentService: CommentService, private httpClient: HttpClient, private route: ActivatedRoute,
              private securityService: SecurityService) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u);

    this.route.params.subscribe(idParam => {
      console.log(idParam);
      this.httpClient.get<Comment[]>('/api/comments', {params: idParam}).subscribe(cl => this.comments = cl);
      console.log('comments get request' + idParam);
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.route.params.subscribe(idParam => {
      this.httpClient.get<Comment[]>('/api/comments', {params: idParam}).subscribe(cl => this.comments = cl);
      console.log('comments get request' + idParam);
    });
  }

  deleteComment(id: number) {
    this.commentService.deleteComment(id);
    document.location.reload();
  }
}
