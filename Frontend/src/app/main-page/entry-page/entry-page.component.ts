import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {EntryDTO} from '../../entryDTO';
import {CommentDTO} from '../../commentDTO';
import {SecurityService} from '../../security.service';
import {CommentService} from './comments/comment.service';
import {User} from '../../user';

@Component({
  selector: 'app-entry-page',
  templateUrl: './entry-page.component.html',
  styleUrls: ['./entry-page.component.css']
})
export class EntryPageComponent implements OnInit {

  private sessionUser: User | null;

  private entryDTO: EntryDTO;

  private test: number;

  private commentDTO: CommentDTO = {
    entryId: 0,
    text: '',
    userName: ''
  };

  constructor(private route: ActivatedRoute, private httpClient: HttpClient, private securityService: SecurityService,
              private commentService: CommentService) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u);

    this.route.params.subscribe(idParam => {
      this.httpClient.get<EntryDTO>('/api/entry', {params: idParam}).subscribe(e => this.entryDTO = e);
    });
  }

  addComment() {
    this.commentDTO.entryId = this.entryDTO.id;
    this.commentDTO.userName = this.securityService.getUserName();
    this.commentService.addComment(this.commentDTO);
    this.commentDTO = {entryId: 0, userName: '', text: ''};
    document.location.reload();
  }

}
