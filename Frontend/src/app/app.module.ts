import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { GoogleMapComponent } from './main-page/google-map/google-map.component';
import { EntriesComponent } from './main-page/entries/entries.component';
import { GoogleMapsModule } from '@angular/google-maps';
import { SessionUserComponent } from './session-user/session-user.component';
import { LoginComponent } from './login/login.component';
import {FormsModule} from '@angular/forms';
import { RegisterComponent } from './register/register.component';
import { MainPageComponent } from './main-page/main-page.component';
import { MapInfoWindowComponent } from './main-page/google-map/map-info-window/map-info-window.component';
import { CreateEntryComponent } from './create-entry/create-entry.component';
import { CommentsComponent } from './main-page/entry-page/comments/comments.component';
import { EntryPageComponent } from './main-page/entry-page/entry-page.component';
import {ProfileComponent} from './profile/profile.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { EditEntryComponent } from './edit-entry/edit-entry.component';


@NgModule({
  declarations: [
    AppComponent,
    GoogleMapComponent,
    EntriesComponent,
    SessionUserComponent,
    LoginComponent,
    RegisterComponent,
    MainPageComponent,
    MapInfoWindowComponent,
    CreateEntryComponent,
    CommentsComponent,
    EntryPageComponent,
    ProfileComponent,
    AboutUsComponent,
    EditEntryComponent
  ],
  imports: [
    BrowserModule,
    GoogleMapsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
