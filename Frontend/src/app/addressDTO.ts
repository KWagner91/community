export interface AddressDTO {
  street: string;
  sNumber: string;
  postalcode: string;
  city: string;
}
